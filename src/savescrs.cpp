// ------------------------------------------------------------------
// savescrs.cpp
// ------------------------------------------------------------------
// This saves screenshoots of the current screen
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2004, Kronoman
// ------------------------------------------------------------------

#include "savescrs.h"

// ------------------------------------------------------------------
// This captures the screenshoot, using the file format 
// of file extension (can be PCX, BMP, TGA)
//
// Will use [name][hex number].[extension] for the name
// The hex number starts counting at hex_start
// Will *not* overwrite files 
// ------------------------------------------------------------------
void save_screenshoot(char *name, int hex_start, char *extension)
{
	char fname[2048];
	BITMAP *bmp;
	PALETTE pal;
	int i = 0;
	
	// will try at least 4096 numbers from hex_start before giving up trying to save
	do 
	{
		usprintf(fname, "%s%x.%s", name, hex_start+i, extension);
		i++;
	} 
	while (exists(fname) && i < 4096);
	
	get_palette(pal);
	bmp = create_sub_bitmap(screen, 0, 0, SCREEN_W, SCREEN_H);
	save_bitmap(fname, bmp, pal);
	destroy_bitmap(bmp);
}
