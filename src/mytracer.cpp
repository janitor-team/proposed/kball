// ------------------------------------------------------------------
// mytracer.cpp
// ------------------------------------------------------------------
// This implements a tracer for debugging the game
// Basically, it records events on disk, so we can trace where the hell
// the bastard is crashing!
// ------------------------------------------------------------------
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// ------------------------------------------------------------------

#include "mytracer.h"

bool CMyTracer::DISABLE_TRACE = false;


CMyTracer::CMyTracer()
{
	// nothing to do	
}

CMyTracer::~CMyTracer()
{
	// nothing to do
}

void CMyTracer::add(string txt)
{
	if (DISABLE_TRACE) return;
	
	FILE *fp;
	fp = fopen(TRACE_SAVE_IN_FILE, "a");
	if (fp == NULL) return; // crap! can't save
	
	fprintf(fp,"%s\n", txt.c_str());
	fclose(fp);
}

void CMyTracer::add(const char *msg, ...)
{
	if (DISABLE_TRACE) return;
	
	char buf[4096];	

	 /* parse the variable parameters */
	 va_list ap;
	 va_start(ap, msg);
	 	vsprintf(buf, msg, ap); // this is ANSI, POSIX, I hope... :O
	 va_end(ap);
	
	this->add(string(buf));
}

void CMyTracer::reset()
{
	if (DISABLE_TRACE) return;
	
	FILE *fp;
	fp = fopen(TRACE_SAVE_IN_FILE, "w");
	if (fp == NULL) return;

	fclose(fp);
}
