// -----------------------------------------------
// Sound System
// By Kronoman
// Copyright (c) 2004, Kronoman
// In loving memory of my father
// -----------------------------------------------
// sound.cpp
// -----------------------------------------------
// This system wraps around Allegro and DUMB,
// so I can use the high level sound routines,
// global adjust volume, disable volume, etc
// -----------------------------------------------
// The formulae for the sound volume is:
//
// 255 -- D (digital volume)
// V ---- X1  ==> X1 = (V * D) / 255
//
// 255 -- G (global volume, final result)
// X1 --- X2 ==> X2 = (X1 * G) / 255
//
// ==> X2 = (( (V * D) / 255) * G ) / 255
//
// For the music:
// 255 -- 1.0f
// X2 --- X3 ==> X3 = (X2 * 1.0f) / 255.0f ==> X2 / 255.0f
// -----------------------------------------------

#include <allegro.h> // Allegro : http://alleg.sf.net/
#include <aldumb.h>  // DUMB : http://dumb.sf.net/
#include "sound.h"

// -----------------------------------------------
// static members and methods
// -----------------------------------------------

bool CSoundWrapper::global_enabled = true; // enables the sound system ?
int CSoundWrapper::global_volume = 255;

void CSoundWrapper::global_set_volume(int v)
{
	if ( v < 0 || v > 255) 
		return;
	
	global_volume = v;
}

int CSoundWrapper::global_get_volume()
{
	return global_volume;
}

void CSoundWrapper::global_set_enabled(bool s)
{
	global_enabled = s;
}

bool CSoundWrapper::global_is_enabled()
{
	return global_enabled;
}

// -----------------------------------------------
// class methods
// -----------------------------------------------


CSoundWrapper::CSoundWrapper()
{
	duh = NULL;
	dp = NULL;
	volume_d = 255;
	volume_m = 255;
	enabled = true;
}

CSoundWrapper::~CSoundWrapper()
{
	// nothing to do for now
}

// -----------------------------------------------
// digital sample playing
// -----------------------------------------------
int CSoundWrapper::play_sample(const SAMPLE *spl, int vol, int pan, int freq, int loop)
{
	if (spl == NULL || volume_d < 1 || global_volume < 1 || !enabled || !global_enabled)
		return -1; // error man :P

	int v = (((vol * volume_d) / 255) * global_volume ) / 255;

	return ::play_sample(spl, v, pan, freq, loop); // call real allegro function
}

// -----------------------------------------------
// volume digital
// -----------------------------------------------
void CSoundWrapper::set_volume_d(int v)
{
	if (v < 0 || v > 255)
		return ;

	this->volume_d = v;
}

int CSoundWrapper::get_volume_d()
{
	return this->volume_d;
}

// -----------------------------------------------
// music
// -----------------------------------------------

// -----------------------------------------------
// call this before playing, dat should be a pointer to a music object of a datafile
// dat should be something like:
//      DATAFILE *dat = load_datafile("smurf.dat");
//      DUH *myduh = (DUH *)dat[GAME_MUSIC].dat;
// -----------------------------------------------
void CSoundWrapper::music_load(DUH *dat)
{
	// DEBUG -- revisar esto, o algo
	duh = dat;
}

// -----------------------------------------------
// start playing
// -----------------------------------------------
void CSoundWrapper::music_start()
{
	if (duh == NULL)
		return ;
	
	if (!enabled || !global_enabled)
		return;

	dp = al_start_duh(duh, 2, 0, ((float)(volume_m / 255) * global_volume ) / 255.0 , 4096, 11025);
}

// -----------------------------------------------
// pause playback
// -----------------------------------------------

void CSoundWrapper::music_pause()
{
	if (duh == NULL || dp == NULL)
		return ;

	al_pause_duh(dp);
}

// -----------------------------------------------
// resume playback
// -----------------------------------------------

void CSoundWrapper::music_resume()
{
	if (duh == NULL) // no hay musica
		return ;
	
	if (dp == NULL) // no estaba tocando
	{
		music_start();
		return;
	}
		
	al_resume_duh(dp);
}

// -----------------------------------------------
// poll music
// _must_ be called at regular intervals to hear the music
// -----------------------------------------------
void CSoundWrapper::music_poll()
{
	if (duh == NULL)
		return ;

	if (dp == NULL)
		music_start();

	if (enabled && global_enabled && global_volume > 0)
		al_poll_duh(dp);
	else
		music_stop();
}

// -----------------------------------------------
// stop playing
// -----------------------------------------------
void CSoundWrapper::music_stop()
{
	if (duh == NULL || dp == NULL)
		return ;

	al_stop_duh(dp);

	dp = NULL;
}

// -----------------------------------------------
// volume music
// -----------------------------------------------
void CSoundWrapper::set_volume_m(int v)
{
	if ( v < 0 || v > 255)
		return ;

	this->volume_m = v;

	if (dp != NULL)
		al_duh_set_volume(dp, ((float)(volume_m / 255) * global_volume ) / 255.0 );
}

int CSoundWrapper::get_volume_m()
{
	return volume_m;
}

// -----------------------------------------------
// for all the object
// -----------------------------------------------
void CSoundWrapper::set_enabled(bool s)
{
	this->enabled = s;
}

bool CSoundWrapper::is_enabled()
{
	return this->enabled;
}

