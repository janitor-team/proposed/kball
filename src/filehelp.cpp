// -------------------------------------------------------- 
// filehelp.cpp
// -------------------------------------------------------- 
// Copyright (c) 2003, 2004, Kronoman
// In loving memory of my father 
// -------------------------------------------------------- 
// I use this for reach the datafile searching in common paths
// Is specially useful for datafiles in Windows, because most 
// of the time Windows don't start the program in the executable path, 
// and the program is unable to find the datafile.
// --------------------------------------------------------

#include <allegro.h>
#include "filehelp.h"
//#include <stdio.h>

// --------------------------------------------------------
// This checks for the filename in several places.
// Returns where the filename is located in buffer
// buffer should be a 2048 bytes char
// If the file is not found, return the filename...
// --------------------------------------------------------
char *where_is_the_filename(char *buffer, const char *filename)
{
char str[2048], str2[2048]; // buffer for path making

// check in current executable path
get_executable_name(str, 2048);
replace_filename(str2, str, filename, 2048);

if (! exists(filename) )
 {
	if (exists(str2)) 
	{
		usprintf(buffer,"%s", str2);
		
		//printf("--> %s\n", buffer);
		
		return buffer;
	}
	else
	{
		get_executable_name(str, 2048);
		replace_filename(str, str, "", 2048);
		if (! find_allegro_resource(str2, filename, get_extension(filename), NULL, NULL, NULL, str, 2048) )
		{
			usprintf(buffer,"%s", str2);
			
			//printf("--> %s\n", buffer);
			
			return buffer;
		}
	}
 }

// default
usprintf(buffer,"%s", filename);

//printf("--> %s\n", buffer);

return buffer;
}
