// -----------------------------------------------
// stats.cpp
// -----------------------------------------------
// Statistics of gameplay for KBall
// -----------------------------------------------
// By Kronoman
// Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

#include "stats.h"

CGameStats::CGameStats()
{
	reset();
}

CGameStats::~CGameStats()
{
	// nothing to do
}

void CGameStats::reset()
{
	h = s = m = score = blost = 0;
}

void CGameStats::print(BITMAP *bmp, int y, int color, int bg, FONT *f)
{
	//textprintf_ex(bmp, f, 0, y+=text_height(f)*2, color, bg, "Statistics");
	
	textprintf_centre_ex(bmp, f, bmp->w/2, y+=text_height(f), color, bg, "TOTAL TIME");
	textprintf_centre_ex(bmp, f, bmp->w/2, y+=text_height(f), color, bg, "%4d:%02d:%02d", h,m,s);
	
	textprintf_centre_ex(bmp, f, bmp->w/2, y+=text_height(f)*2, color, bg, "TOTAL SCORE");
	textprintf_centre_ex(bmp, f, bmp->w/2, y+=text_height(f), color, bg, "%010ld", score);
	
	textprintf_centre_ex(bmp, f, bmp->w/2, y+=text_height(f)*2, color, bg, "BALLS LOST");
	textprintf_centre_ex(bmp, f, bmp->w/2, y+=text_height(f), color, bg, "%3d", blost);
}

void CGameStats::add_time(int h1, int m1, int s1)
{
	s += s1;
	
	m += s / 60;
	s = s % 60;

	m += m1;
	
	h += m / 60;
	m = m % 60;

	h += h1;
}

