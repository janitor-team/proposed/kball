// -----------------------------------------------
// particle.cpp
// -----------------------------------------------
// Many particle types, designed to be used with
// my particle manager.
// ----------------------------------------------- 
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

#include <allegro.h>
#include "particle.h"

// -----------------------------------------------
// Spark particle render
// -----------------------------------------------
void CSparkParticle::render_particle(BITMAP *bmp, int xd, int yd)
{
	line(bmp, (int)x - xd, (int)y - yd, (int)(x + dx*scale_spark - xd), (int)(y + dy*scale_spark - yd), color);
}

// -----------------------------------------------
// Circle particle render
// -----------------------------------------------
void CCircleParticle::render_particle(BITMAP *bmp, int xd, int yd)
{
	circlefill(bmp, (int)x - xd, (int)y - yd, radius, color);
}

// -----------------------------------------------
// Rect particle render
// -----------------------------------------------
void CRectParticle::render_particle(BITMAP *bmp, int xd, int yd)
{
	rectfill(bmp, (int)x - xd, (int)y - yd, (int)(x + size - xd), (int)(y + size - yd), color);
}

// -----------------------------------------------
// Text particle render
// -----------------------------------------------
void CTextParticle::render_particle(BITMAP *bmp, int xd, int yd)
{
	text_mode(-1); // DEBUG -- TEXT ALWAYS TRANSPARENT :O
	textout_centre(bmp, font_text, text_to_show.c_str(), (int)x - xd, (int)y - yd, color);
}

// -----------------------------------------------
// Explosive text particle
// -----------------------------------------------
bool CExplosiveTextParticle::update_logic(CParticleManager &particle_manager)
{
	// when life == 1, spawn particles
	if (life == 1)
	{
		int yt=0,xt=0;
		
		while ( (yt+=rand()%3+1) < text_height(font_text) )
		{
			while ( (xt+=rand()%3+1) <  text_length(font_text, text_to_show.c_str() ) )
			{
				particle_manager.add_particle(new CCircleParticle((int)x+xt, (int)y+yt, (float)((rand()%800)-400)/100.0,(float)((rand()%800)-400)/100.0 , color, rand()%5+10, rand()%2+1));
			}
		}
	}
	
	return CTextParticle::update_logic(particle_manager);
}

// -----------------------------------------------
// Bitmap particle render
// -----------------------------------------------
void CBitmapParticle::render_particle(BITMAP *bmp, int xd, int yd)
{
	if (spr == NULL) return;
	draw_sprite(bmp, spr, (int)x - xd - spr->w/2, (int)y - yd - spr->h/2);
}

// -----------------------------------------------
// Rotating bitmap particle render & logic
// -----------------------------------------------
bool CRotoBitmapParticle::update_logic(CParticleManager &particle_manager)
{
	angle += angle_speed;
	if (angle < 0.0) angle = 255.0;
	if (angle > 255.0) angle = 0.0;
	
	return CBaseParticle::update_logic(particle_manager); 
}

void CRotoBitmapParticle::render_particle(BITMAP *bmp, int xd, int yd)
{
	if (spr == NULL) return;
	pivot_sprite(bmp, spr, (int)x - xd, (int)y - yd, spr->w/2, spr->h/2, ftofix(angle));
}

