// ------------------------------------------------------------------
// cwdata.h
// ------------------------------------------------------------------
// This is a wrapper over a datafile.
// Basically, it wraps a datafile in resources like bitmaps, 
// sound and fonts in a way that can be requested
// and used by the program, just requesting them by name
// ------------------------------------------------------------------
// By Kronoman
// In loving memory of my father
// Copyright (c) 2003, Kronoman
// ------------------------------------------------------------------
// Upgraded in January 2004, based on skin.cpp of my simple GUI manager
// ------------------------------------------------------------------
#ifndef CWDATAFILE_H
#define CWDATAFILE_H

// Allegro
#include <allegro.h>

// STL stuff
#include <map> // sweet key-data container
#include <iostream>
#include <string>
using namespace std;


// This is the type of data that I use for the Map that cache the datafile resources
typedef map<string,DATAFILE *> DatafileCacheMap;

class CWDatafile
{
	public:
		CWDatafile();
		CWDatafile(const char *filename);
	
		~CWDatafile();
	
		void nuke_datafile(); // this frees the memory used by the datafile (and the cache, so, it resets the datafile)
		
		bool load_datafile(const char *filename); // this loads a datafile from a datafile in hard disk, returns TRUE if fails
		
		void do_cache(); // this do the map cache of resources, is automatically called when needed
	
		void *get_resource_dat(const string resource_name); // This is BETTER, returns directly the data or NULL on error
		void *get_resource_dat(const char *resource_name); // This is BETTER, returns directly the data or NULL on error
		
		DATAFILE *get_resource(const string resource_name); // The hot stuff: get resources, or NULL on error (or exception, if set)
		DATAFILE *get_resource(const char *resource_name);
		
		DATAFILE *get_whole_datafile(); // This returns a pointer to the whole loaded DATAFILE (in case that you need it for something) 
		
		void dump_debug_datafile_data(); // debug function, shows all loaded on console output
		
		// for all the class
		static void set_die_on_failure(bool b) { CWDatafile::die_on_failure = b; } // die on failure?	
			
	private:
		DATAFILE *datafile; // datafile in RAM
		DatafileCacheMap datafile_cache_map; // STL map that does the cache of the datafile resources
	
		// for all the class
		static bool die_on_failure; // if a error ocurs, kill the app? (die with error message)
};

#endif
