// ------------------------------------------------------------------
// backgbmp.h
// System for handling level backgrounds
//
//
// NOTE ; SYSTEM VERY SLOW, IMPROVE IT WITH A CACHE, ETC!
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2004, Kronoman
// ------------------------------------------------------------------

#ifndef BACKGBMP_H
#define BACKGBMP_H

#include <allegro.h>


// background
class CBackground
{
	public:
		CBackground();
		~CBackground();
		
		BITMAP *get_background(char *filename, int index); // gets the background from file, or a default background otherwise
		void free_memory(); // releases memory and returns to default background
	
	private:
		BITMAP *bmp_in_ram;
		DATAFILE *data_loaded;
		BITMAP *bmp_default;
		
		// cache stuff
		char file_loaded[1024]; // wich file we have in memory
		int index_loaded;
		

};

#endif
