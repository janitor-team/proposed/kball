// -------------------------------------------------------- 
// filehelp.h
// -------------------------------------------------------- 
// Copyright (c) 2003, 2004, Kronoman
// In loving memory of my father 
// -------------------------------------------------------- 
// I use this for reach the datafile searching in common paths
// Is specially useful for datafiles in Windows, because most 
// of the time Windows don't start the program in the executable path, 
// and the program is unable to find the datafile.
// --------------------------------------------------------


#ifndef FILEHELP_H
#define FILEHELP_H

char *where_is_the_filename(char *buffer, const char *filename);

#endif
