// ------------------------------------------------------------------
// Some miscellaneous definitions
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2003, Kronoman
// ------------------------------------------------------------------

#ifndef KRONO_MISC_DEF
#define KRONO_MISC_DEF

// this file has the tile set for the map
#define TILESET_FILE "tileset.dat"
// this file has the sprites, sound, backgrounds, etc
#define SPRITES_FILE "sprites.dat"

#endif

