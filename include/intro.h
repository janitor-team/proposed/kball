// ------------------------------------------------------------------
// intro.h
// ------------------------------------------------------------------
// This is the intro / exit secuence for the game - everything hardcoded, sorry
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2004, Kronoman
// ------------------------------------------------------------------
#ifndef KBALL_INTRO_H
#define KBALL_INTRO_H

#include <allegro.h>

void kball_do_the_intro();
void kball_do_the_exit();

#endif
