// -----------------------------------------------
// partmang.h
// -----------------------------------------------
// Particle manager, to control particles in the game
// Also, implementation of base particle
// ----------------------------------------------- 
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

#ifndef PARTMANG_H
#define PARTMANG_H

#include <allegro.h>

#include <list> // STL container for the particles (yeah, sue me, I'm not doing my own linked list :P)
#include <iterator>
using namespace std;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
// The base particle, a dot
// This particle must be used as base 
// for making other types of particles, using
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
class CParticleManager; // I need this here, so my particle 'knows' the manager and can send messages to him :D

class CBaseParticle
{
	public:
		CBaseParticle();
		CBaseParticle(float x1, float y1, float dx1, float dy1, int color1, int life1);
		virtual ~CBaseParticle();
		
		// the update logic receives the manager, so the particle can add new particles, if desired
		virtual bool update_logic(CParticleManager &particle_manager); // updates particle's logic, must return TRUE if particle is dead (particle WILL be _deleted_ from memory by manager if dead)
		
		virtual void render_particle(BITMAP *bmp, int xd, int yd); // renders particle on bmp, displaced by x,y
		
	// all particle properties are public, for easy modification (yeah, I know, poor OO design, don't bug me)
		float x, y, dx, dy; // particle x,y, and direction (acceleration in x,y)
		int color; // particle color (in Allegro's makecol format)
		int life; // remaining life of particle
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
// The particle manager
// This handles a bunch of particles
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -   
class CParticleManager
{
	public:
		CParticleManager();
		~CParticleManager();
		
		void nuke_particle_list(); // this will free all memory used by particles, and particles itself.
		
		void add_particle(CBaseParticle *node); // adds a particle to manager (the particle WILL be nuked automatically when the manager object gets deleted)
		
		void update_logic(); // updates logic of all particles

		void render(BITMAP *bmp, int x, int y); // renders particles on bmp, displaced by x,y

		// some public data
		float add_x, add_y; // displace particles on x, y, this can be used to simulate wind/gravity effects (is add to x,y of all particles)
		float add_dx, add_dy; // this is add to dx,dy of all particles (can be used to simulate acceleration, etc)
		
	private:
		list <CBaseParticle *> particle_list; // storage for pointers to particles that I must manage
};
	
#endif
