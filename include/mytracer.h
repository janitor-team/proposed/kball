// ------------------------------------------------------------------
// mytracer.h
// ------------------------------------------------------------------
// This implements a tracer for debugging the game
// Basically, it records events on disk, so we can trace where the hell
// the bastard is crashing!
// ------------------------------------------------------------------
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// ------------------------------------------------------------------

#ifndef MYTRACER_H
#define MYTRACER_H

#include <string>
using namespace std;

// yeah, sue me, I'm using C functions (I have my reasons, so, be nice and STFU)
#include <stdio.h>
#include <stdarg.h> // for the variable argument list

// file to save the trace (filename, 8.3 chars to keep compatibility with DOS)
#define TRACE_SAVE_IN_FILE "tracelog.txt"

class CMyTracer
{
	public:
		CMyTracer();
		~CMyTracer();
	
		void add(string txt);
		void add(const char *msg, ...); // format like printf :O
		
		void reset(); // reset the file where we are tracing (OVERWRITES THE FILE!)
	
		// NOTE: this is configuration for all class objects (notice the 'static')
		static bool DISABLE_TRACE; // define this to true to DISABLE the logging
};


#endif
