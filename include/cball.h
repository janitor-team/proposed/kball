// ------------------------------------------------------------------
// cball.h
// ------------------------------------------------------------------
// This class is the ball of the player
// By Kronoman - In loving memory of my father
// Copyright (c) 2003,2004, Kronoman
// ------------------------------------------------------------------

#ifndef CBALL_H
#define CBALL_H

#include <allegro.h>

#include "tmap.h" // I need to know the map where I move
#include "control.h" // the controller class
#include "partmang.h" // particle manager 
#include "particle.h"
#include "sound.h"

// values that "int CBall::update_logic()" may return
	// all OK
	#define CBALL_IS_FINE 0
	// I'm so dead :'(
	#define CBALL_IS_DEAD 1
	// I'm over the level's exit ;^D
	#define CBALL_EXIT_LEVEL 2


// lives by default for SINGLE level
#define BALL_LIVES 3
// lives by default for campaign mode
#define BALL_LIVES_CAMPAIGN 7

// acceleration
#define BALL_SPEED_X  0.65
#define BALL_SPEED_Y  0.65
#define BALL_SPEED_Z  0.35

// radius of the ball (diameter will be this size * 2
#define BALL_RADIUS 25

// max speed in any axis
#define BALL_MSPEED 10

// friction
#define N_BALL_SPEED_X 0.1
#define N_BALL_SPEED_Y 0.1
// gravity
#define N_BALL_SPEED_Z 0.15

// max Z value of the ball (jump limit)
#define BALL_MAX_Z 15.0

// min value of Z, below here, is DEAD
#define BALL_MIN_Z -15.0

class CBall
{
	public:
		CBall();
		~CBall();

		void draw(BITMAP *bmp, int sx, int sy); // draw the ball
		
		int update_logic(); // updates the logic, returns status of the ball
		
		// -- Data members (all public) --
		// this vars are public to let easy access to them, (yes, I know that this broke the encapsulation)
		
		float x, y, z, dx, dy, dz;  // pos x,y,z ;  speed dx, dy, dz
		
		BITMAP *spr; // sprite (texture of sphere, will be the size of texture too)
		BITMAP *spr_shadow; // shadow mask for the sphere texture
		
		BITMAP *spr_cache; // cache for sphere rotaded sprite
		
		float anglex, angley; // angle of rotation of the sprite (to animate it)
		
		int lives; // lives left
		long int score; // score
	
		CTMap *ctmap; // pointer to the map where the ball moves, the ball NEEDS to know the map
		
		CParticleManager *particle_manager; // pointer to game's particle manager, the ball NEEDS to add particles when get pickups, etc

		CController control; // controller for gameplay
		
		CSoundWrapper soundw; // sound system
};

#endif


