// ------------------------------------------------------------------
// savescrs.h
// ------------------------------------------------------------------
// This saves screenshoots of the current screen
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2004, Kronoman
// ------------------------------------------------------------------

#ifndef SAVESCRS_H
#define SAVESCRS_H

#include <allegro.h>
void save_screenshoot(char *name, int hex_start, char *extension);

#endif
