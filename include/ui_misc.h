// ------------------------------------------------------------------
// ui_misc.h
// ------------------------------------------------------------------
// This are some miscellaneous helper functions, mainly used by the kernel UI interface
// ------------------------------------------------------------------
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// ------------------------------------------------------------------

#ifndef UI_MISC_H
#define UI_MISC_H

#include <allegro.h>
void ui_misc_dark_bmp(BITMAP *bmp);
void ui_misc_text_out_shadow(BITMAP *bmp, FONT *f, const char *s, int x, int y, int cs, int ct);
void ui_misc_wait_for_input();

#endif
