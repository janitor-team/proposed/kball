// ------------------------------------------------------------------
// tmap.h
// ------------------------------------------------------------------
// This handles the tile map for the ball game
// ------------------------------------------------------------------
// By Kronoman - In loving memory of my father
// Copyright (c) 2003, Kronoman
// ------------------------------------------------------------------

#ifndef TMAP_H
#define TMAP_H

#include <allegro.h>

#include "cwdata.h" // For data handling/loading of tile data

// square map size (fixed, sorry ; also, don't get this > 256 because I use chars to determine numbers :P)
#define TMAP_SIZE 256

// sprite size of each tile
#define TMAPS_W 64
#define TMAPS_H 64

// map layers (currently, leave this at 2)
#define MAP_LAYERS 2

// ============================================================================
// This is a single tile class (defines different kinds of tiles, 
// each one of this is 1 tile class)
// ============================================================================
class CTileClass
{
	public:
		CTileClass();
		~CTileClass();
		void draw(BITMAP *bmp, int x, int y, bool draw_grid, int layer); // draws the tile on bmp at x,y (pixel coordinates)
		
		void load_from_config(int nt, CWDatafile *data); // this loads the tile 'nt' (1.255) from a previously set config
		void save_to_config(int nt); // this saves parameters to a previously set config *FILE* (not memory pointer)
		
		// all data public
		BITMAP *spr; // sprite
		char spr_name[512]; // this is to keep sprite name, is just to let the thing save itself when needed (as in a map editor)

		SAMPLE *sound; // sound ; will be played *only* if: a) is a prize and is pickup ; b) is a wall and ball bounces
		char sound_name[512]; // this is to keep sound name, is just to let the thing save itself when needed (as in a map editor)
		
		// physic propertys
		bool exit_level; // this kind of tile is exit level (default = false)
		bool solid; // this is totally solid -> IS a WALL? (the ball bounces against it, check bounce factor) (default = false)
		float bounce_factor; // a value _positive_ that is 'bounce factor', when solid = true, (default = 0.9)
		float adx, ady,adz; // when the ball goes over this tile, this is _added_ to the dx,dy,dz values of the ball (default = 0.0)
		float mdx, mdy,mdz; // when the ball goes over this tile, this is _multiplied_ to the dx,dy,dz values of the ball (default = 1.0)
		
		// prize propertys, this are for when the tile belongs to 'prize' layer
		int score; // score on pickup
		bool indispensable; // we must pickup it to pass the level
		bool is_a_prize; // this tile is a 'prize' or just a decoration tile
};

// ============================================================================
// This handles a tile map
// ============================================================================
class CTMap
{
	public:
		CTMap();
		~CTMap();

		bool get_tile_walkable(int x, int y, int layer); // this serves for validation purposes (for the ball), will return true if the tile is walkable, false otherwise (outside map counts as non walkable)
		
		int get_tile_type(int x, int y, int layer); // this returns the tile type (1..255), or -1 if outside map, or 0 if empty (if empty, or outside map, ball should fall free to death)
		
		void set_tile_type(int x, int y, int layer, int value); // this sets the tile type at x,y (0..255) x,y are coordinates of the matrix
		
		bool load_map_from_file(const char *filename); // load tile map from file, return true if FAILS, false otherwise
		bool save_map_to_file(const char *filename); // save tile map to file, return true if FAILS, false otherwise
		
		bool load_tile_set_from_file(char *filename); // loads a tile set from a DATAFILE (filename is the name of the datafile), return true if FAILS, false otherwise
		bool save_tile_set_config_to_file(char *filename); // saves the tile set configuration to a text file (not the bitmaps!)
		
		void empty_the_map(); // resets all map to 0s
		
		void draw_map(BITMAP *bmp, int ix, int iy, int iw, int ih, int layer, bool draw_grid); // draws a zone of the map, coordinates in pixels
		
		void free_memory(); // releases the memory, auto called on destructor
		
		void update_logic(); // updates the logic of the map (animations, time remaining, etc)
		
		// -- data, all public for faster access --
		int tile_map[TMAP_SIZE][TMAP_SIZE][MAP_LAYERS]; // tile map loaded, is divided in layers, 0 = floor, 1 = prizes, DON'T USE MORE (is like this for future ampliation only)
		
		CTileClass tile_set[256]; // tile class loaded  (each tilem[][] is a index in this array) ; NOTE: 0 is reserved for empty tile index!!!, so this goes 1..255 for valid tiles 
		
		CWDatafile tile_set_data; // this is the data loaded from file (has the sprites, etc of the tile_set array)

		int background_index; // ID index of background, ranges from 0...255
		int music_index; // ID index of the music, ranges from 0..255
		
		int prize_map_indispensable; // ammount of indispensable items on map (if > 0, you can't leave the map)
		int time_m; // minutes (0..255)  left to try to collect items and reach exit
		int time_s;  // seconds (0..59)  left to try to collect items and reach exit
		int timer_tick_rate; // set this to measure timer tick rate, otherwise, the time will run wild (ex: if update logic is called 30 times by second, then set this to 30)
		int curr_tick; // internal current tick measurer, when reaches 0, time_s --;
		
		int sxp, syp; // starting position for the player (is the first 2 bytes of the map file, 0..255, 0..255)
};

#endif


