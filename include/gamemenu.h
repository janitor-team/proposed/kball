// -----------------------------------------------
// gamemenu.h
// -----------------------------------------------
// This has the menu of the Kball game
// ----------------------------------------------- 
// Developed By Kronoman - Copyright (c) 2004
// In loving memory of my father
// -----------------------------------------------

#ifndef GAMEMENU_H
#define GAMEMENU_H

#include <allegro.h>

#include <time.h> // for showing time, and also, for a little surprise ;P

#include "cwdata.h" // datafile wrapper
#include "qmenu.h" // menu system
#include "gkernel.h" // main game loop 
#include "mapedit.h" // built in map editor -:^D
#include "gerror.h" // error reporting
#include "partmang.h" // particle manager, for nice particle effects of background
#include "particle.h" // many particle types
#include "mytracer.h" // this shit keeps crashing, I need to debug it... dammit!
#include "sound.h" //  sound system

class CGameMenu
{
	public:
		CGameMenu();
		~CGameMenu();
		
		void do_main_menu();
		void do_file_level_selector();
		void do_options_menu();
		void do_about_stuff();
		
	private:
		BITMAP *menu_back; // doble buffer bitmap for menu
		CWDatafile menu_datafile; // datafile for menu data
		CMapEditor map_editor; // amazing map editor(r)(c)(tm)
		CMyTracer mtracer; // tracer for debug log
		CGameKernel game_kernel; // Game kernel :^O
		CSoundWrapper soundw; // sound system
};

	void do_main_menu();
	void do_file_level_selector();
#endif
