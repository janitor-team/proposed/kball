@echo off

echo KBall Source Code
echo.
echo KBall's website: http://kball.sf.net/


if [%1] == [linux]   goto linux
if [%1] == [djgpp]   goto djgpp
if [%1] == [mingw32] goto mingw32
if [%1] == [test] goto test
goto help


:test
REM Does a test to see if the platform can compile
REM You first need to configure the platform
if exist target.os goto targetok
    echo Before testing, you first must configure your platform.
goto help

:targetok
echo Testing, please wait...
make test
if not errorlevel 0 goto testfail
if not exist test.run goto testfail

    echo.
    echo * SUCESS *
    echo Congratulations, the test compiled!
    echo.
	echo NOTE: You need Allegro 4.1.15 or better to compile KBall

goto testdone

:testfail
    echo.
    echo * ERROR *
    echo.
    echo The compilation returned a error!
    echo Check that:
    echo (*) You have all compiler tools installed (gcc,make,etc...)
    echo (*) You have Allegro 4.1.15 or better properly installed (http://alleg.sf.net/)
	echo (*) You have DUMB 0.9.2 or better properly installed (http://dumb.sf.net/)
	echo.

:testdone
    echo Cleaning the test...
    make cleantest
    
goto done

:djgpp
echo Configuring for DOS/djgpp...
echo # Warning! This file will be overwritten by configuration routines! > target.os
echo TARGET=DJGPP>> target.os
goto done


:mingw32
echo Configuring for Windows/Mingw32...
echo # Warning! This file will be overwritten by configuration routines! > target.os
echo TARGET=MINGW32>> target.os
goto done


:linux
echo Configuring for Linux/GCC...
echo # Warning! This file will be overwritten by configuration routines! > target.os
echo TARGET=LINUX>> target.os
goto done


:help
echo Usage: fix platform
echo.
echo Where platform is one of: djgpp, mingw32 or linux. 
echo.
echo NOTICE:
echo You can also call: fix test
echo to check if your system can compile this programs.
echo.
echo To compile KBall you need Allegro 4.1.15 or better, and DUMB 0.9.2 or better
echo http://alleg.sf.net/
echo http://dumb.sf.net/
echo.
goto end

:done
echo Done!

:end
